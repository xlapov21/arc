 #include <stdio.h>
 int main() {
    int num_elements, i, j;
     scanf("%d", &num_elements);
    int a[num_elements];
    for(i = 0 ; i < num_elements; i++) {
        scanf("%d", &a[i]);
    }
    int num_sorted = 0;
    while (num_sorted!=num_elements-1) {
       num_sorted = 0;
       for(i = 0 ; i < num_elements - 1 ; i++) {
           int t;
	   if(a[i] > a[i+1]) {
              t = a[i];
              a[i] = a[i+1] ;
              a[i+1] = t;
           }
	   else {
	      num_sorted++;
	   }
       }
    }
    for(i = 0 ; i < num_elements; i++) {
        printf("%d ", a[i]);
    }
}
