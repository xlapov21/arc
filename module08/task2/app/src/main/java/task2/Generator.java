package task2;

class Generator {

    private long timeout;
    private double[] array;
    private int n;
    private long timeToGenerate;

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public double[] getArray() {
        if (generatedInTime()) {
            return null;
        }
        return this.array;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public long getTimeToGenerate() {
        return timeToGenerate;
    }

    public Generator(long timeout, int n) {
        this.timeout = timeout;
        this.n = n;
    }

    public boolean generatedInTime() {
        return timeToGenerate > timeout;
    }



    public void generate() {
        array = new double[n];
        long time = System.currentTimeMillis();

        for (int i = 0; i < n; i++) {
            array[i] = Math.random();
        }

        this.timeToGenerate = System.currentTimeMillis() - time;
    }
}