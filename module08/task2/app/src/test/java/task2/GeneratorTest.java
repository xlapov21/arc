package task2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GeneratorTest {
    @Test void generatorMax() {
        Generator generator = new Generator(100, 50000000);
        generator.generate();
        assertEquals(true, generator.generatedInTime());
    }

    @Test void generatorMin() {
        Generator generator = new Generator(10000, 100);
        generator.generate();
        assertEquals(false, generator.generatedInTime());
    }

    @Test void generatorMoreTime() {
        Generator generator = new Generator(100, 50000000);
        generator.generate();
        assertEquals(null, generator.getArray());
    }
}