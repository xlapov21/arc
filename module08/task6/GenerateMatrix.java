import java.util.Scanner;

public class Generate_Matrix {
    public static int[][][] generate_matrix(int num, int n, int m) {
        int[][][] matrix = new int[num][n][m];
        for (int i = 0; i < num; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < m; k++) {
                    double qwerty = Math.random() * 100;
                    matrix[i][j][k] = (int) qwerty;
                }
            }
        }
        return matrix;
    }

    public static long multiply_matrix(int[][] massive_1, int[][] massive_2) throws Exception{
        if (massive_1.length == massive_2[0].length) {
            int[][] result_array = new int[massive_1.length][massive_2[0].length];
            for (int i = 0; i < massive_1[0].length; i++) {
                for (int j = 0; j < massive_2.length; j++) {
                    for (int y = 0; y < massive_2[0].length; y++) {
                        result_array[i][j] = result_array[i][j] + massive_1[i][y] * massive_2[y][j];
                    }
                }
            }
            long time = System.currentTimeMillis();
            return time;
        } else {
            throw new Exception();
        }
    }

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите количество матриц, которые вы хотите сгенерировать");
        int num = sc.nextInt();
        System.out.println("Введите количество столбцов в матрице");
        int n = sc.nextInt();
        System.out.println("Введите количество строк в матрице");
        int m = sc.nextInt();
        System.out.println("Сгенерированные матрицы");

        int[][][] array_massive = generate_matrix(num, n, m);
        for (int i = 0; i < num; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < m; k++) {
                    System.out.print(array_massive[i][j][k] + " ");
                }
                System.out.println();
            }
            System.out.println("--------------------");
        }

        long time_start = System.currentTimeMillis();

        for (int i = 0; i < num - 1; i++) {
            for (int j = i + 1; j < num; j++) {
                System.out.println((multiply_matrix(array_massive[i], array_massive[j]) - time_start) + " мс");
            }
        }

    }
}
