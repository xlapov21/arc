import java.util.Scanner;

public class Menu {
    public static void main(String[] args) {
        Node node1 = new Node(2, new FactorialAction());
        Node node21 = new Node(1, new PowerAction());
        Node node22 = new Node(1, new SumAction());
        Node node31 = new Node(0, new PrintHi());
        Node node32 = new Node(0, new PrintBye());
        node1.children[0] = node21;
        node1.children[1] = node22;
        node21.setParent(node1);
        node22.setParent(node1);
        node21.children[0] = node31;
        node22.children[0] = node32;
        node31.setParent(node21);
        node32.setParent(node22);
        Node node = node1;
        char emptyActionChar = '-';
        char a = emptyActionChar;
        while (!(a == '0' && !node.hasParent())) {
            System.out.println(node.getQuestion());
            for (int i = 0; i < node.getChildren().length; i++) {
                System.out.println(MyView.press + (i + 2) + MyView.then + node.getChildren()[i].getAction().getDescription());
            }
            Scanner sc = new Scanner(System.in);
            a = sc.nextLine().charAt(0);
            if (a == '1') {
                int n = node.getAction().getNumberArguments();
                int[] array = new int[n];
                for (int i = 0; i < n; i++) {
                    array[i] = sc.nextInt();
                }
                node.doAction(array);
            } else if (a == '0') {
                if (node.hasParent()) {
                    node = node.goToParent();
                    a = emptyActionChar;
                }
            } else if (a > '1' && a <= '9') {
                node = node.goToChild(a - 50);
            }
        }
    }
}
