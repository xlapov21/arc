public class MyView {
    public static String goToPast = "Нажмите 0, чтобы поднятся выше";
    public static String press = "Нажмите ";
    public static String then = ", чтобы ";
    public static String doSmth = "Нажмите \"d\", чтобы ";
    public static String powerActionDescription = "вывести 2 в степени натурального числа";
    public static String factorialActionDescription = "вывести факториал числа";
    public static String sumActionDescription = "вывести сумму двух чисел";
    public static String printHiDescription = "вывести привет";
    public static String printByeDescription = "вывести пока";
}
