public class Node {
    public String question;
    public Action action;
    public Node parent;
    public Node[] children;

    public Node(int n, Action action) {
        this.children = new Node[n];
        this.action = action;
        this.question = MyView.goToPast + "\n";
        this.question += MyView.press + "1" + MyView.then + action.description;
    }

    public String getQuestion() {
        return this.question;
    }

    public Node getParent() {
        return this.parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Node[] getChildren() {
        return this.children;
    }

    public Action getAction() {
        return this.action;
    }

    public boolean hasNext() {
        return this.children.length != 0;
    }

    public boolean hasParent() {
        return parent != null;
    }

    public Node goToChild(int n) {
        if (n >= children.length) try {
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.children[n];
    }

    public Node goToParent() {
        if (this.parent == null) try {
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.parent;
    }

    public void doAction(int ... v) {
        this.action.doSmth(v);
    }
}