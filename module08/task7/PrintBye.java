public class PrintBye extends Action{
    public PrintBye() {
        this.description = MyView.printByeDescription;
    }

    public void doSmth(int ... v) {
        System.out.println("Bye");
    }

    public int getNumberArguments() {
        return 0;
    }

    public String getDescription() {
        return this.description;
    }
}
