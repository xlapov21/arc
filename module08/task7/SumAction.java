public class SumAction extends Action{
    public SumAction() {
        this.description = MyView.sumActionDescription;
    }

    public void doSmth(int ... v) {
        System.out.println("Сумма двух чисел равна: " + (v[0] + v[1]));
    }

    public int getNumberArguments() {
        return 2;
    }

    public String getDescription() {
        return this.description;
    }
}
