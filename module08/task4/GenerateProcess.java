import java.util.Random;
import java.util.Scanner;

public class GenerateProcess {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double threshold = sc.nextDouble();
        double coefficient = sc.nextDouble();
        int max_time = sc.nextInt();

        long time_start = System.currentTimeMillis();

        double[][] massive = new double[2][100];
        Random random = new Random();

        int i = 0;
        double max = - coefficient * threshold;
        double min = - (coefficient * threshold);

        q1:while (max_time > System.currentTimeMillis() - time_start) {
            if (random.nextBoolean() && i < massive[0].length) {
                double buffer = (Math.random() * ((max - min) + 1)) + min;
                if (buffer < threshold) {
                    massive[0][i] = System.currentTimeMillis() - time_start;
                    massive[1][i] = buffer;
                    i++;
                } else {
                    break q1;
                }
            }
        }

        for (int j = 0; j < massive[0].length; j++) {
            if (massive[1][j] != 0) {
                System.out.println(massive[0][j] + ": " + massive[1][j]);
            }
            else {
                break;
            }
        }
        System.out.println();
    }
}
