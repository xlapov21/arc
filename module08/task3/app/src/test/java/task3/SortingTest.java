package task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class SortingTest {
    @Test void createSortingArray() {
        double[] a1 = {5, 4, 8, 25, 1, 9, 64};
        double[] a2 = {1, 4, 5, 8, 9, 25, 64};
        assertArrayEquals(a2, Sorting.createSortedArray(a1));
    }

    @Test void sorting() {
        double[] a1 = {5, 4, 8, 25, 1, 9, 64};
        Sorting.sort(a1);
        double[] a2 = {1, 4, 5, 8, 9, 25, 64};
        assertArrayEquals(a2, a1);
    }

    @Test void createSortingArrayOne() {
        double[] a1 = {1};
        double[] a2 = {1};
        assertArrayEquals(a2, Sorting.createSortedArray(a1));
    }

    @Test void sortingOne() {
        double[] a1 = {1};
        Sorting.sort(a1);
        double[] a2 = {1};
        assertArrayEquals(a2, a1);
    }

    @Test void createSortingArrayBlank() {
        double[] a1 = {};
        double[] a2 = {};
        assertArrayEquals(a2, Sorting.createSortedArray(a1));
    }

    @Test void sortingBlank() {
        double[] a1 = {};
        Sorting.sort(a1);
        double[] a2 = {};
        assertArrayEquals(a2, a1);
    }
}
