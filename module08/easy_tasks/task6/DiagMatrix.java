import java.util.Scanner;

public class DiagMatrix {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите количество столбцов");
        int n = sc.nextInt();

        int[][] massive = new int[n][n];
        System.out.println("Введите матрицу");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                massive[i][j] = sc.nextInt();
            }
        }

        System.out.println("-----------");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i > j) {
                    System.out.print(massive[i][j] + " ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
}
