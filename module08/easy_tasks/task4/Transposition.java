class Transposition {
    public static void main(String[] args) {
        int[][] massive = {{1,2,3},
                {4,5,6}};
        int[][] new_massive = new int[massive[0].length][massive.length];

        for (int i = 0; i < new_massive.length; i++) {
            for (int j = 0; j < new_massive[0].length; j++) {
                new_massive[i][j] = massive[j][i];
            }
        }

        for (int i = 0; i < new_massive.length; i++) {
            for (int j = 0; j < new_massive[0].length; j++) {
                System.out.print(new_massive[i][j]+" ");
            }
            System.out.println();
        }
    }
}
