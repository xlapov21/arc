package task5;

class SymbolsInLine {

  public static int symbolInLine(String line, char symbol) {
    char[] arrayLine = line.toCharArray();
    int counter = 0;
    for (int i = 0; i < arrayLine.length; i++) {
      if (arrayLine[i] == symbol) {
        counter++;
      }
    }
    return counter;
  }
}
