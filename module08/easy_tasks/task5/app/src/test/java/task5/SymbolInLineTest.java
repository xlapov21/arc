package task5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SymbolInLineTest {
    @Test void symbolInLine() {
        String str = "aaaabbbaaaa";
        char sym = 'a';
        assertEquals(8, SymbolsInLine.symbolInLine(str, sym));
    }

    @Test void symbolInLineOne() {
        String str = "a";
        char sym = 'a';
        assertEquals(1, SymbolsInLine.symbolInLine(str, sym));
    }

    @Test void symbolInLineBlank() {
        String str = "";
        char sym = 'a';
        assertEquals(0, SymbolsInLine.symbolInLine(str, sym));
    }

    @Test void symbolInLineAbsence() {
        String str = "aaaabbbaaaa";
        char sym = 'c';
        assertEquals(0, SymbolsInLine.symbolInLine(str, sym));
    }
}
