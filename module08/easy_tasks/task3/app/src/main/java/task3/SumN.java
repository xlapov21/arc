package task3;

import java.math.BigInteger;

class SumN {

    public static BigInteger sum(BigInteger n) {
        BigInteger sum = BigInteger.valueOf(0);
        sum = (BigInteger.ONE).add(n).divide(BigInteger.valueOf(2)).multiply(n);
        return sum;
    }
}
