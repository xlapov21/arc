import java.util.Scanner;

public class Segments {

    public static int rnd(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int diap = sc.nextInt();
        int shift = sc.nextInt();
        int[][] array = new int[n * 2][2];
        int max = shift + diap;
        int min = shift - diap;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 2; j++) {
                array[i][j] = (int) rnd(min, max);
            }
            for (int j = 2; j < 4; j++) {
                array[i + n][j - 2] = (int) rnd(min, max);
            }
        }

        int num = 0;

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if ((array[j][0] > array[i][0] && array[j][0] < array[i][1]) || (array[j][1] > array[i][0] && array[j][1] < array[i][1])) {
                    num++;
                } else if (array[j][0] < array[i][0] && array[j][1] > array[i][1]) {
                    num++;
                }
            }
        }
        System.out.println(num);
    }
}
