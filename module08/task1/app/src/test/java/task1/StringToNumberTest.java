package task1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringToNumberTest {
    @Test void symbolInLineAbsence() {
        String str = "100+10-5";
        int res = 105;
        assertEquals(res, StringToNumber.remakeStringToNumber(str));
    }
}
